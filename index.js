const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url === "/index.html") {
      fs.readFile("./index.html", "UTF-8", (err, data) => {
        if (err) {
          res.writeHead(500, { "Content-Type": "text/plain" });
          res.end("Internal Server Error");
        }
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(data);
      });
    } else if (req.url === "/string.json") {
      fs.readFile("string.json", (err, data) => {
        if (err) {
          res.writeHead(500, { "Content-Type": "text/plain" });
          res.end("Internal Server Error");
        }
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(data);
      });
    } else if (req.url === "/uuid") {
      const content = uuidv4();
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(content);
    } else if (req.url.startsWith("/status/")) {
      const statusCode = parseInt(req.url.split("/")[2]);
      res.end(`Status Code ${statusCode}`);
    } else if (req.url.startsWith("/delay/")) {
      const delaySeconds = parseInt(req.url.split("/")[2]);
      setTimeout(() => {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Status Code 200");
      }, delaySeconds * 1000);
    } else {
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.end("404 Not Found");
    }
  }
});
server.listen(8000, () => {
  console.log(`Server running at port:8000`);
});
